#######################################
# Effect

set(slide_SOURCES
    main.cpp
    slide.cpp
    springmotion.cpp
)

kconfig_add_kcfg_files(slide_SOURCES
    slideconfig.kcfgc
)

kwin4_add_effect_module(kwin4_effect_slide ${slide_SOURCES})
target_link_libraries(kwin4_effect_slide PRIVATE
    ukui-kwineffects

    KF5::ConfigGui
)
