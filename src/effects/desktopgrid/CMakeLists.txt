# SPDX-FileCopyrightText: 2021 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
#
# SPDX-License-Identifier: BSD-3-Clause

set(desktopgrid_SOURCES
    main.cpp
    desktopgrideffect.cpp
)

kconfig_add_kcfg_files(desktopgrid_SOURCES
    desktopgridconfig.kcfgc
)


kwin4_add_effect_module(kwin4_effect_desktopgrid ${desktopgrid_SOURCES})

target_link_libraries(kwin4_effect_desktopgrid PRIVATE
    ukui-kwineffects

    KF5::ConfigGui
    KF5::GlobalAccel
    KF5::I18n

    Qt::Quick
    )



install(DIRECTORY qml DESTINATION ${KDE_INSTALL_DATADIR}/ukui-kwin/effects/desktopgrid)
